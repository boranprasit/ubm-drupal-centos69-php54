#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

#exec /usr/sbin/apachectl -DFOREGROUND

if [ -f /mnt/apache/conf/httpd.conf ]; then
	rm -f /etc/httpd/conf/httpd.conf
	ln -s /mnt/apache/conf/httpd.conf /etc/httpd/conf/httpd.conf
else
	cp /etc/httpd/conf/httpd.conf.orig /etc/httpd/conf/httpd.conf	
fi

/usr/sbin/apachectl
/bin/bash
