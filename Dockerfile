
# Modified by Charoun Boranprasit
FROM centos:centos6
MAINTAINER Tuan Doan <tuan.doan@ubm.com>

ENV code_root /code
ENV httpd_conf ${code_root}/httpd.conf
ENV my_cnf $(code_root)/mysql/my.cnf
# Set the Drush version.
ENV DRUSH_VERSION 7.3.0

RUN mkdir /temp
WORKDIR /temp

RUN yum -y update && yum clean all
RUN yum -y install wget epel-release

RUN wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
RUN rpm -Uvh remi-release-6.rpm

RUN yum install -y wget zlib-devel httpd yum-utils mysql mysql-server openssl-devel vim git nano curl devtoolset-2 epel-release which python26 gpg
RUN yum groupinstall -y development 'development tools'


RUN yum install  -y \
	php54-php \
	php54-php-cli \
	php-cli \
	php54-php-gd \
	php54-php-ldap \
	php54-php-pear \
	php54-php-mysqlnd \
	php54-php-mbstring \
	php54-php-mcrypt \
	php54-php-pdo \
	bzip2 \
	tar \
	php-curl \
        php-xml \
	php-zip \
	php-fileinfo \
	php-xdebug

RUN yum clean all

# Install V8
ADD v8js.x64.tar.gz /temp/
WORKDIR /temp/v8js.x64

RUN cp lib/* /lib64/
RUN cp v8js.so /opt/remi/php54/root/usr/lib64/php/modules/
RUN echo "extension=v8js.so" > /opt/remi/php54/root/etc/php.d/v8js.ini

RUN cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.orig


# Install RVM
RUN curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
RUN curl -L get.rvm.io | bash -s stable

# Install Tiller through RVM
RUN /bin/bash -l -c "rvm install 2.4.0 && \
        gem install bundler --no-ri --no-rdoc && \
        rvm use 2.4.0 --default && \
        gem update --system && \
        gem install capistrano -v 2.15.9 && \
        gem install capistrano-ext && \
        gem install tiller"

#
# ... Rest of Dockerfile goes here ...
#

# Run in a login shell so RVM can set up Ruby paths etc.
CMD ["/bin/bash" , "-l" , "-c" , "tiller"]

ADD . $code_root

RUN test -e $httpd_conf && echo "Include $httpd_conf" >> /etc/httpd/conf/httpd.conf

EXPOSE 80
CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN composer global require drush/drush:7.*

RUN ln -s /usr/local/bin/composer /usr/bin/composer
RUN composer global require drush/drush:"$DRUSH_VERSION" --prefer-dist

RUN git clone https://github.com/drush-ops/drush.git /usr/local/src/drush
RUN cd /usr/local/src/drush && git checkout $DRUSH_VERSION
RUN ln -s /usr/local/src/drush/drush /usr/bin/drush
RUN cd /usr/local/src/drush && composer update && composer install

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]
